﻿app.controller("mainController", function ($scope, $document, $http) {
    $scope.devinfo = {};

    $http.get('devinfo/information.json').then(function (data) {
        $scope.devinfo = data.data;
        console.log($scope.devinfo);
    }, function (reason) {
        console.log(reason);
    });
});