﻿var app = angular.module("adminSoftApp", ["ngRoute"], function ($compileProvider) {
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|javascript):/);
});

app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    $locationProvider.hashPrefix('');

    $routeProvider
        .when("/",
            {
                templateUrl: "/dashboard/principal.html"
            })
        .otherwise({
            templateUrl: "/"
        });
}]);